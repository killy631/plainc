/**
	@bried Client 
*/

#include "stdafx.h"
#include "handlers.h"

#define CMD_EXIT 10

/**
	Displays trace on stderr
*/
static void
trace_error(const char* msg)
{
	fprintf(stderr, "[ERROR]: %s\n", msg);
}


static int
display_help(const char* line)
{
	printf("AVAILABLE COMMANDS:\n\n");

	printf("\tHELP - displays this text\n");
	printf("\tEXIT - exits to shell\n");
	printf("\tPUT key value - adds a key value pair to the db\n");
	printf("\tLIST - show available keys in the db\n");
	printf("\tGET key - extracts value by its key\n");
	printf("\tERASE key - kill value by its key\n");

	return OK;
}


typedef int (*FN_CMD_HANDLER)(const char* cmd_text);

typedef struct _handler
{
	char			code[10];
	FN_CMD_HANDLER	phandler;
} HANDLER;


static HANDLER k_handlers[] = 
{
	{"HELP",	display_help},
	{"PUT",		hand_call_server},
	{"GET",		hand_call_server},
	{"ERASE",	hand_call_server},
	{"LIST",	hand_call_server},
};



static int
process_command(const char* cmd_line)
{
	int		i;
	int		exit_code = CMD_EXIT;
	char*	line;

	if(NULL == cmd_line)
		return ERR_INVALID_ARG;

	line = strdup(cmd_line);

	// Special case - exit to shell
	{
		char verb[10];
		char rest[255];
		sscanf(line, "%s %s", &verb, &rest); 

		strupr(verb);

		for(i = 0; i < sizeof(k_handlers) / sizeof(HANDLER); i++)
		{
			if(strcmp(verb, "EXIT") == 0)
			{
				exit_code = CMD_EXIT;
				goto exit;
			}

			if(strcmp(verb, k_handlers[i].code) == 0)
			{
				exit_code = k_handlers[i].phandler(line);
				if(OK == exit_code)
					goto exit;
			}
		}

		trace_error("Sorry - command unknown");
		exit_code = 0;
	}

exit:
	free(line);

	return exit_code;
}


int 
main(int argc, char* argv[])
{
	printf("* Tiny database client. Copyright by Arik (C), 2017 **\n\n");
	printf("Please enter command below. To get complete list of available command, input HELP\n");

	while(TRUE)
	{
		char line[256];
		int  rv;

		printf("\n> ");
		if(NULL != fgets(line, sizeof(line) - 1, stdin))
		{
			
			rv = process_command(line);
			if(rv == CMD_EXIT)
				return 0; // Normal termination
		}
		else
		{
			trace_error("Internal error");
			return 10;
		}
	}

	return 0;
}

