/**
	@bried We dont need it on Linux.
*/

#ifndef _STDAFX_H_
#	define _STDAFX_H_ 1

#	ifdef _WIN32
#		include	"targetver.h"
#	endif // _WIN32

#include <stdio.h>
#include <tchar.h>

#include <fcntl.h>
#include <io.h>
#include <errno.h>
#include <malloc.h>

#include "../core/defines.h"

#endif // _STDAFX_H_

