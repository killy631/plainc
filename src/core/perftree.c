#include "preftree.h"
#include "utils.h"
#include <string.h>

//////////////////////////////////////////////////////////////////////////
//	private scope

/**
*	@brief ��������� ������������ ����� � ��������� � ������ �������.
*/
#define GET_INDEX(c) ((unsigned char)(c) - 'a')


/**
*	@brief ���������� �������� � ������������ ����
*/
static ERR_CODE
set_value(TREE_NODE * node, const char * value)
{
	size_t str_size = 0;
	ERR_CODE result = OK;

	if (NULL == node)
	{
		result = ERR_NULL_POINTER;
		goto exit;
	}

	if (node->has_value)
	{
		// ������� ������ �� ������������� ����� �������� (�������� ������)
	}
	if (NULL == value || 0 == *value)
	{
		node->has_value = FALSE;
		node->value.offset = 0;
		node->value.length = 0;

		goto exit;
	}

	str_size = strlen(value);
	// TODO
	node->value.offset = DUMMY_INT;//	������� � ����� ��������
						   //	���������� � ���� (���������� � �����)
	node->value.length = str_size;
	node->has_value = TRUE;

exit:
	return result;
}


/**
*	@brief ������� ����� ������
*	@return ���������� ����� ���� ��� 0
*	�������� ������ ������ ���� � ������ ��������
*/
static TREE_NODE *
make_node(char letter)
{
	size_t size = sizeof(TREE_NODE);
	TREE_NODE * result = NULL;

	if (!CHECK_LETTER_LOWERCASE(letter))
		return result;

	result = (TREE_NODE*)malloc(size);

	if (result)
	{
		memset(result, 0, size);
		result->type = letter;
		result->file_offset = NO_KEY_OFFSET;
	}

	return result;
}

/**
*	@brief ���������, �� ���� �� ���� ���������
*	@return ���������� TRUE, ���� ���� �� ����� �����
*/
static BOOL
check_last_node(TREE_NODE * node)
{
	int i;
	for (i = 0; i < TREE_VERTEX_COUNT; i++)
		if (node->next[i])
			FALSE;

	return TRUE;
}

/**
*	@brief ��������� ��������� �� ��������� ����
*	@return ���������� ���������� ������, ��������������� �������� �����
*	�������� ������ ������ ���� � ������ ��������
*/
static TREE_NODE *
get_next_node(TREE_NODE * node, const char letter)
{
	if ( NULL == node || !(CHECK_LETTER_LOWERCASE(letter)) )
		return 0;

	return node->next[GET_INDEX(letter)];
}

/**
*	@brief ����� ��������� ���� �������� �����
*	@return ���������� ��������� ��� 0, ���� �� �������
*	���� ������ ���� � ������ ��������.
*/
static TREE_NODE *
get_node_by_key(TREE_NODE * node, char * str_key)
{
	unsigned char uc = *str_key;
	if (0 == uc || NULL == node) // ����� �����������, ���� �� ������
		return 0;

	if (0 == *(++str_key)) // �������� ����� �����
		return node;

	if (uc == node->type)
		return get_node_by_key(get_next_node(node, *str_key), str_key);
// 	else if (ROOT_TYPE == node->type)
// 		return get_node_by_key(get_next_node(node, *(--str_key)), str_key); // ������������ �����

	return NULL;
}

/**
*	@brief �������� � ������ ���� �� ��������� ���� �������� �����
*	@return ���������� ��������� ��� 0, ���� ��������� ������
*	���� ������ ���� � ������ ��������.
*/
static TREE_NODE *
create_path_by_key(TREE_NODE * node, char * str_key)
{
	unsigned char uc = *str_key;
	TREE_NODE * next_node = 0;

	if (0 == uc || NULL == node) // ����� �����������, ���� �� ������
		return NULL;

	if (0 == *(++str_key)) // �������� ����� �����
	{
		next_node = make_node(uc);
		node->next[GET_INDEX(uc)] = next_node;
		node->has_children = TRUE;

		return next_node;
	}
	
	next_node = get_next_node(node, (ROOT_TYPE != node->type ? *str_key : uc));

	if (next_node)
	{
		return create_path_by_key(next_node, str_key);
	}
	else
	{
		next_node = make_node(uc);
		node->next[GET_INDEX(uc)] = next_node;
		node->has_children = TRUE;

		return create_path_by_key(next_node, str_key);
	}

	return NULL;
}

static ERR_CODE
delete_node(TREE_NODE * node)
{
	ERR_CODE result = OK;
	
	return result;
}
/*
static ERR_CODE
release_tree_root(TREE_NODE * root)
{
	ERR_CODE result = OK;
	// TODO
	return result;
}
*/
//////////////////////////////////////////////////////////////////////////
//	public scope

ERR_CODE
tree_find(TREE_NODE * root, const char * key, TREE_NODE ** found_node)
{
	ERR_CODE result = OK;
	char * str_key = strdup(key);

	if (NULL == root || NULL == key || NULL == str_key)
	{
		result = ERR_NULL_POINTER;
		goto exit;
	}

	result = util_validate_and_transform(str_key);
	if (OK != result)
		goto exit;
	
	*found_node = get_node_by_key( (ROOT_TYPE == root->type ? get_next_node(root, *key) : root), str_key );
	if (NULL == *found_node)
		result = ERR_NOT_FOUND;

exit:
	free(str_key);
	return result;
}


ERR_CODE
tree_insert(TREE_NODE * root, const char * key, const char * value )
{
	ERR_CODE result = OK;
	char * str_key = strdup(key);
	TREE_NODE * new_node = NULL;

	if (NULL == root || NULL == key || NULL == str_key)
	{
		result = ERR_INVALID_ARG;
		goto exit;
	}
	// ���� ���� ��� ���������� ������� ��� ����������� ��������
	result = tree_find(root, key, &new_node);
	if (OK == result && new_node)
	{
		set_value(new_node, value);
	}
	else
	{
		result = util_validate_and_transform(str_key);
		if (OK != result)
			goto exit;

		new_node = create_path_by_key(root, str_key);
		result = set_value(new_node, value);
	}

exit:
	free(str_key);
	return result;
}


ERR_CODE
tree_erase(TREE_NODE * root, const char * key)
{
	ERR_CODE result = OK;
	TREE_NODE * new_node = 0;

	if (0 == key || NULL == root)
	{
		result = ERR_INVALID_ARG;
		goto exit;
	}

	result = tree_find(root, key, &new_node);
	if (OK == result && new_node)
	{
		result = set_value(new_node, "");
	}

exit:
	return result;
}


ERR_CODE
tree_clean(TREE_NODE * root) // TODO!!!
{
	ERR_CODE result = OK;
	TREE_NODE * next_node = 0;
	int i;
	
	if (NULL == root)
	{
		result == ERR_INVALID_ARG;
		goto exit;
	}

	if (root->has_children)
	{
		for (i = 0; i < TREE_VERTEX_COUNT; i++)
		{
			next_node = root->next[i];
			if (next_node)
			{
				if (!root->has_value)
				{
					// TODO ������� ��� ���� �� �����

					free(next_node);
					root->next[i] = 0;
				}
				else
					tree_clean(next_node);
			}
		}

		if (check_last_node(root))
			root->has_children = FALSE;
	}
	else
	{
		if (!root->has_value)
		{
			// TODO ������� ��� ���� �� �����

			free(root);	
		}
	}

exit:
	return result;
}


ERR_CODE
tree_init(TREE_NODE ** out_root)
{
	int				i;
	unsigned char	c = 'a';
	TREE_NODE *		new_root = NULL;
	TREE_NODE *		subroot = NULL;

	if (NULL == out_root || *out_root) // ��� ���������� �� ������ ���� ������� ������
		return ERR_INVALID_ARG;

	new_root = make_node(c);

	if (NULL == new_root)
		return ERR_NULL_POINTER;

	new_root->type = ROOT_TYPE;

	for (i = 0; i < TREE_VERTEX_COUNT; i++)
	{
		subroot = make_node(c + i);
		if (subroot)
		{
			new_root->next[i] = subroot;
			
			subroot = NULL;
		}
	}

	new_root->has_children = TRUE;

	*out_root = new_root;

	return OK;
}

/*
ERR_CODE
tree_release(TREE_NODE * root)
{
	ERR_CODE result = OK;
	int i;
	
	for (i = 0; i < TREE_VERTEX_COUNT; i++)
	{
		
	}
	
	return result;
}
*/
