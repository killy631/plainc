/**
*	@brief TREE - ���������� ������
*	������ �������� ������������������� ������ ��������� � ASCII.
*	������������ ������ ������, ��� ��������.
*/
#ifndef PREFTREE_H
#define PREFTREE_H

#include "defines.h"
#include <stdlib.h>


/**
*	@brief ���������� ��������� ����� ������ -
*	����� ����� ���������� ��������
*/
#define TREE_VERTEX_COUNT	26

/**
*	@brief ������ EOF ��� �������� �� �����
*/
#define NO_KEY_OFFSET	((size_t)-1)

/**
*	@brief ����������, ��� ������ ���� �������� ������ ������
*/
#define ROOT_TYPE	0x01 // ������ ��������� ������������� �����, ����� ������ ������ �������������� ��� ���������


/**
*	@brief �������� �������� �� ������ ����� � ������ ������
*/
typedef struct _value
{
	size_t	offset;
	size_t	length;
} TREE_NODE_VALUE;

/**
*	@brief ���� ����������� ������
*/
typedef struct _node
{
	char			type;
	BOOL			has_value;
	BOOL			has_children;
	size_t			file_offset;
	struct _node *	next[TREE_VERTEX_COUNT];
	TREE_NODE_VALUE value;
} TREE_NODE;


// typedef struct _tree
// {
// 	TREE_NODE roots_array[TREE_VERTEX_COUNT];
// } TREE;

//////////////////////////////////////////////////////////////////////////
//	functional

/**
*	@brief ������������� ������� ��������� ������ - ��������� root-�� ��� ���� ���� ��������
*	@return ���������� OK ��� ��� ������
*/
ERR_CODE tree_init(TREE_NODE ** out_root /* + file handlers */);

/**
*	@brief ���������� ������ � ������� ���������� ������
*	@return ���������� OK ��� ��� ������
*/
ERR_CODE tree_release(TREE_NODE * root);

/**
*	@brief ����� ��������� ���� �������� �����
*	@return ���������� ��������� ��� 0, ���� �� �������
*/
ERR_CODE tree_find(TREE_NODE * root, const char * key, TREE_NODE** found_node);

/**
*	@brief ������� ����
*	@return ���������� ��������� ��� 0, ���� �� �������
*/
ERR_CODE tree_insert(TREE_NODE * root, const char * key, const char * value);

/**
*	@brief �������� ����
*/
ERR_CODE tree_erase(TREE_NODE * root, const char * key);

/**
*	@brief �������� "������" �����
*/
ERR_CODE tree_clean(TREE_NODE * root);

#endif // PREFTREE_H
