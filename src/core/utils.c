#include "utils.h"


ERR_CODE
util_validate_and_transform( char * in_out_str )
{
	unsigned char c = *in_out_str;

	if (0 == in_out_str)
		return ERR_NULL_POINTER;

	while (0 != c)
	{
		// ������� � ������ �������
		if (c < 0x5B)
		{
			c += 0x20;
			*in_out_str = c;
		}

		if (CHECK_LETTER_LOWERCASE(c))
			c = *(++in_out_str);
		else
			return ERR_INVALID_STRING_KEY;
	}

	return OK;
}
