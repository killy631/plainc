/**
*	@brief useful utiles
*/
#ifndef _UTILS_H_
#define _UTILS_H_

#include "defines.h"

/**
*	@brief ���������, ��� ������ ��� ����� ������� ��������
*/
#define CHECK_LETTER_LOWERCASE(c)	( (const unsigned char)(c) > 0x60 && (const unsigned char)(c) < 0x7B )

/**
*	XXX: ����� ������� ������ �� ���� � ������ ��������.
*	@brief ��������� ������������ ����� � ��������� � ������ �������.
*	@return �� ��� ������, ERR_INVALID_STRING_KEY - ���� ���� �� ������������� �����������
*/
ERR_CODE util_validate_and_transform(char * in_out_str);


#endif // _UTILS_H_
