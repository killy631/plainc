// Server.c
#include <stdio.h>

#include "../core/preftree.h"
static TREE_NODE * global_trie = NULL;

int main(int argc, char* argv[])
{
	TREE_NODE * pNode = 0;
	int i = -1;
	
	i = tree_init(&global_trie);
	
	i = tree_insert(global_trie, "a", "qwertyuiop");
	i = tree_insert(global_trie, "Bod", "vbmn");
	i = tree_find(global_trie, "bOD", &pNode);
	i = tree_insert(global_trie, "bod", 0);
	i = tree_find(global_trie, "bOD", &pNode);
	i = tree_clean(global_trie);
	i = tree_find(global_trie, "bO", &pNode);
	i = tree_find(global_trie, "a", &pNode);
	i = tree_erase(global_trie, "a");
//	i = tree_find(global_trie, "a", &pNode);
	
	printf("hello server! \n");

	return 0;
}

/*
int main(int argc, char* argv[])
{
	TREE * global_trie = malloc(sizeof(TREE));
	Node * pNode = 0;
	int i = -1;

	init_trie(global_trie);

	i = insert(global_trie, "Bod", "vbmn");
	i = find(global_trie, "bOD", &pNode);
	i = insert(global_trie, "bod", 0);
	i = find(global_trie, "bOD", &pNode);
	i = clean_tree(global_trie);
	i = find(global_trie, "bO", &pNode);
	i = find(global_trie, "bOD", &pNode);

	printf("hello server! \n");

	free(global_trie);
	return 0;
}
*/
