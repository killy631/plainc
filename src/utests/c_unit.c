#include "stdafx.h"
#include "c_unit.h"
#include <malloc.h>
#include "console.h"
#include "test_context.h"

static void
exec_test(UNIT_TEST* test)
{
	ctx_clear();

	__try
	{
		test->test_function();
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		ctx_set_except_state();
	}
}


static int
max_spaces_count(UNIT_TEST suite[], int test_count)
{
	size_t maxlen = 0;
	int i;

	for(i = 0; i<test_count; i++)
	{
		size_t hintlen = strlen(suite[i].hint);
		if(hintlen > maxlen)
			maxlen = hintlen;
	}

	return (int)maxlen + 1;
}


static void
print_with_align(char* hint, int max_hint_len)
{
	int space_count = max_hint_len - (int)strlen(hint);
	char* spaces = NULL;

	printf(hint);

	spaces = (char*) malloc(space_count+1);
	memset(spaces, 0x20, space_count);
	spaces[space_count] = 0;

	printf("%s", spaces);
	free(spaces);
}


int 
run_test_suite(UNIT_TEST* suite, int test_count)
{
	int				i, result = TRUE, spaces_count;
	CONSOLE_CONTEXT	ctx;

	if(NULL == suite)
		return -1;
	
	spaces_count = max_spaces_count(suite, test_count);
	printf("\nRunning test suite...\n\n");

	ctx_clear();
	store_default_context(&ctx);

	for(i=0; i<test_count; i++)
	{
		restore_color(&ctx);
		print_with_align(suite[i].hint, spaces_count);

		exec_test(&suite[i]);	
		result = result && !ctx_is_failed();

		switch(ctx_get_state())
		{
		case STATE_NORMAL:
			set_color(&ctx, FOREGROUND_GREEN);
			printf("OK\n");
			break;

		case STATE_EXCEPTION:
			set_color(&ctx, FOREGROUND_RED);
			printf(ctx_get_last_msg());
			break;

		case STATE_FAILED:
			set_color(&ctx, FOREGROUND_MAGENTA);
			printf(ctx_get_last_msg());
			break;
		}
	}

	restore_color(&ctx);
	if(result)
		printf("\n\nSuite succeeded");
	else
		printf("\n\nSuite failed");

	ctx_clear();
	return result;
}
