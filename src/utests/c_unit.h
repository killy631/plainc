/*
**  Plain C unit test framework.
**  Suite runner
**
**  Peter A.Smirnoff
** 
**  $Id: c_unit.h 38 2009-12-30 11:29:56Z peter $
**
*/
 
#ifndef C_UNIT
#define C_UNIT

#include "stdafx.h"

typedef void (*TEST_FUNCTION)();

typedef struct
{
	TEST_FUNCTION test_function;
	char* hint;

} UNIT_TEST;


int		run_test_suite(UNIT_TEST* suite, int test_count);

#endif /* C_UNIT */