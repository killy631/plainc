#include "stdafx.h"
#include "console.h"


void
store_default_context(CONSOLE_CONTEXT* ctx)
{
	CONSOLE_SCREEN_BUFFER_INFO con_buff_info;

	ctx->hconsole = GetStdHandle(STD_OUTPUT_HANDLE);
	memset(&con_buff_info, 0, sizeof(CONSOLE_SCREEN_BUFFER_INFO)); 

	GetConsoleScreenBufferInfo(ctx->hconsole, &con_buff_info);
	ctx->default_text_attr = con_buff_info.wAttributes;
}


void
set_color(CONSOLE_CONTEXT* ctx, int color)
{
	SetConsoleTextAttribute(ctx->hconsole, color | FOREGROUND_INTENSITY );
}


void
restore_color(CONSOLE_CONTEXT* ctx)
{
	SetConsoleTextAttribute(ctx->hconsole, ctx->default_text_attr);
}

