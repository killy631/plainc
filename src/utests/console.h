/*
** Windows console control (colors)
**
** Peter A. Smirnoff
**
** $Id: console.h 38 2009-12-30 11:29:56Z peter $
** 
*/

#ifndef CONSOLE_H
#define CONSOLE_H

#include "stdafx.h"

typedef struct
{
	HANDLE	hconsole;
	WORD	default_text_attr;

} CONSOLE_CONTEXT;

#define FOREGROUND_MAGENTA 5

void	store_default_context(CONSOLE_CONTEXT* ctx);
void	set_color(CONSOLE_CONTEXT* ctx, int color);
void	restore_color(CONSOLE_CONTEXT* ctx);

#endif /* CONSOLE_H */