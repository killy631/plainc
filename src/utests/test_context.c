#include "stdafx.h"
#include "test_context.h"

TEST_CONTEXT g_context;

void 
ctx_clear()
{
	g_context.state = STATE_NORMAL;
	if(g_context.last_error_message)
	{
		free(g_context.last_error_message);
		g_context.last_error_message = NULL;
	}
}


BOOL
ctx_is_failed()
{
	return g_context.state == STATE_FAILED || g_context.state == STATE_EXCEPTION;
}


static char*
new_str(const char* org)
{
	char* result = NULL;
	size_t  dest_size = 0;

	if(!org) return NULL;
	
	dest_size = strlen(org)+1;
	result = (char*) malloc(dest_size);
	strcpy_s(result, dest_size, org);
	
	return result;
}

const char exception_alert[] = "Exception occured\n";

void
ctx_set_except_state()
{
	g_context.state = STATE_EXCEPTION;
	g_context.last_error_message = new_str(exception_alert);
}


void	
ctx_check(BOOL condition, char* hint,  char* func_name, int line)
{
	char*	format = "FAILED : %s in function %s at line %d\n";
	int		tchar_count;
	size_t  buff_size;

	if(condition)
	{
		g_context.state = STATE_NORMAL;
		return;
	}
	else
	{
		g_context.state = STATE_FAILED;
	}
		
	tchar_count = _scprintf(format, hint, func_name, line) + 1;
	buff_size   =  sizeof(char) * tchar_count;
	
	g_context.last_error_message = (char*) malloc(sizeof(char) * tchar_count);
	memset(g_context.last_error_message, 0, tchar_count);
	sprintf_s(g_context.last_error_message, buff_size, format, hint, func_name, line);
}


TEST_STATE
ctx_get_state()
{
	return g_context.state;
}


char*
ctx_get_last_msg()
{
	return g_context.last_error_message;
}
