/*
**  Plain C unit test framework.
**  Signle unit - test context.
**
**  Peter A. Smirnoff
**
**  $Id: test_context.h 38 2009-12-30 11:29:56Z peter $
**
*/

#ifndef TEST_CONTEXT_H
#define TEST_CONTEXT_H

#include "stdafx.h"

typedef enum 
{
	STATE_NORMAL,
	STATE_FAILED,
	STATE_EXCEPTION
} TEST_STATE;

typedef struct
{
	TEST_STATE	state;
	char*		last_error_message;

} TEST_CONTEXT;


void		ctx_clear();
void		ctx_check(BOOL condition, char* hint,  char* func_name, int line);
BOOL		ctx_is_failed();
void		ctx_set_except_state();
TEST_STATE	ctx_get_state();
char*		ctx_get_last_msg();	

#define TEST(condition, hint) ctx_check(condition, hint, __FUNCTION__, __LINE__)
#define FAIL ctx_is_failed()

#endif /* TEST_CONTEXT_H */