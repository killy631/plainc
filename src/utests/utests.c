/**
	@brief Unit test suite
*/
#include "stdafx.h"
#include "c_unit.h"
#include "test_context.h"

static void
empty_test()
{
}


static void
successful_test()
{
	TEST(1==1, "Math problem...");
	TEST(1==1, "Math problem...");
}


static void
failed_test()
{
    BYTE buffer[4] = {0, 123, 22, 255};
    SetLastError(ERROR_FILE_NOT_FOUND);
    
	TEST(1 != 1, "Catastrophic condition...");
}


static void
exceptional_test()
{
#ifdef _WIN32
	RaiseException(ERROR_FILE_NOT_FOUND, 0, 0, NULL);
#endif // _WIN32
}


static void
tree_init_free_test()
{
	TREE root;

	TEST(tree_init(&root) == OK, "Init failure");
	if(FAIL) goto done;


done:;
}


/* 
	Please assure what  TEST_COUNT == the real count of initializers !!! 
*/

#define TEST_COUNT 5
UNIT_TEST  suite[TEST_COUNT] =
{
	{empty_test,		"Test with no body"},
	{successful_test,	"Test alsways OK"},
	{failed_test,		"Test always fails"},
	{exceptional_test,	"Test raises a win32 exception"},
	{tree_init_free_test,  "Tree init-free test"}
};


int 
main(int argc, char* argv[])
{
	BOOL result;
	
	result = run_test_suite(suite, TEST_COUNT);
	return result;
}

